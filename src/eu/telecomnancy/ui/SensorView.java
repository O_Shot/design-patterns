package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class SensorView extends JPanel implements Observer {
    
	private static final long serialVersionUID = 1L;
	private static ISensor sensor;
    private static TemperatureSensorDecorator sensorDeco;

    private static JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    private JButton unit = new JButton("Unit");
    
    SensorCommand sensorOn;
    SensorCommand sensorOff;
    SensorCommand sensorUpdate;
    SensorCommand sensorUnit;
    
    SensorSwitch s = new SensorSwitch();

    public SensorView(ISensor c) throws SensorNotActivatedException {
        SensorView.sensor = c;
        SensorView.sensorDeco = new TemperatureSensorDecorator(SensorView.sensor.getStatus(), SensorView.sensor.getValue());
        ((Observable)SensorView.sensor).addObserver(this);
        this.setLayout(new BorderLayout());
        
        this.sensorOn = new SensorOnCommand(sensor);
        this.sensorOff = new SensorOffCommand(sensor);
        this.sensorUpdate = new SensorUpdateCommand(sensor,sensorDeco);
        this.sensorUnit = new SensorUnitCommand(sensor, sensorDeco);

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	/*
                try {
					s.storeAndExecute(sensorOn);
				} catch (SensorNotActivatedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				*/
            	sensor.on();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	try {
					s.storeAndExecute(sensorOff);
				} catch (SensorNotActivatedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	
            	try {
					s.storeAndExecute(sensorUpdate);
				} catch (SensorNotActivatedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
            	/*
            	sensorDeco.setUnit("°C");
                try {
					sensor.update();
				} catch (SensorNotActivatedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				*/
            }
        });
        
        unit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	try {
					s.storeAndExecute(sensorUnit);
				} catch (SensorNotActivatedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        buttonsPanel.add(unit);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

	@Override
	public void update(Observable temp, Object arg1) {
		try{
            SensorView.value.setText("" + SensorView.sensor.getValue() + SensorView.sensorDeco.getUnit());
        } catch(SensorNotActivatedException e){
        }
	}
	
	public static JLabel getValue() {
		return value;
	}
	
	public static ISensor getSensor() {
		return sensor;
	}
	
	public static TemperatureSensorDecorator getDecorator() {
		return sensorDeco;
	}
}
