package eu.telecomnancy.sensor;

public class SensorOffCommand implements SensorCommand{
	
	private ISensor sensor;
	
	public SensorOffCommand(ISensor sensor) {
		this.sensor = sensor;
	}
	
	public void execute() {
		this.sensor.off();
	}
	
}