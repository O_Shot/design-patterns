package eu.telecomnancy.sensor;

public class SensorOnCommand implements SensorCommand{
	
	private ISensor sensor;
	
	public SensorOnCommand(ISensor sensor) {
		this.sensor = sensor;
	}
	
	public void execute() {
		this.sensor.on();
	}
	
}
