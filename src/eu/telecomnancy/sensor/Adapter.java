/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;

import eu.telecomnancy.sensor.SensorNotActivatedException;
/**
 *
 */
public class Adapter implements ISensor {
    
    LegacyTemperatureSensor oldSensor;
    private double value;
    
    public Adapter(LegacyTemperatureSensor newSensor) {
        oldSensor = newSensor;
    }
    
    /**
     * Enable the sensor.
     */
    public void on() {
        if(!oldSensor.getStatus()) {
        	oldSensor.onOff();
        }
    }

    /**
     * Disable the sensor.
     */
    public void off() {
        if(oldSensor.getStatus()) {
        	oldSensor.onOff();
        }
    }
    

    /**
     * Get the status (enabled/disabled) of the sensor.
     *
     * @return the current sensor's status.
     */
    public boolean getStatus() {
        return oldSensor.getStatus();
    }

    /**
     * Tell the sensor to acquire a new value.
     *
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public void update() throws SensorNotActivatedException {
    	this.value = oldSensor.getTemperature();
    }

    /**
     * Get the latest value recorded by the sensor.
     *
     * @return the last recorded value.
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public double getValue() throws SensorNotActivatedException {
        return this.value;
    }

}
    

