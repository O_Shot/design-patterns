package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.SensorView;

public class SensorUnitCommand implements SensorCommand{
	
	@SuppressWarnings("unused") // sensor is used
	private ISensor sensor;
	private TemperatureSensorDecorator sensorDeco;
	
	public SensorUnitCommand(ISensor sensor, TemperatureSensorDecorator sensorDeco) {
		this.sensor = sensor;
		this.sensorDeco = sensorDeco;
	}
	
	public void execute() throws SensorNotActivatedException {
		sensorDeco.setUnit();
    	try {
			SensorView.getValue().setText("" + sensorDeco.getFactor(SensorView.getSensor().getValue()) + SensorView.getDecorator().getUnit());
		} catch (SensorNotActivatedException e1) {
			e1.printStackTrace();
		}
	}
	
}