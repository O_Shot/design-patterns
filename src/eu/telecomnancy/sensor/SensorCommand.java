package eu.telecomnancy.sensor;

public interface SensorCommand {
	void execute() throws SensorNotActivatedException;
}
