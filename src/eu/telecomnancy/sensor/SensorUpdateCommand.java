package eu.telecomnancy.sensor;

public class SensorUpdateCommand implements SensorCommand{
	
	private ISensor sensor;
	private TemperatureSensorDecorator sensorDeco;
	
	public SensorUpdateCommand(ISensor sensor, TemperatureSensorDecorator sensorDeco) {
		this.sensor = sensor;
		this.sensorDeco = sensorDeco;
	}
	
	public void execute() throws SensorNotActivatedException {
		sensorDeco.setUnit("°C");
        sensor.update();
	}
	
}