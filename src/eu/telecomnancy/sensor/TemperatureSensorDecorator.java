package eu.telecomnancy.sensor;

public class TemperatureSensorDecorator extends TemperatureSensor {
	private String unit = "°C";
	private double factor = 1;
	
	public TemperatureSensorDecorator(boolean state, double value) {
		super(state, value);
	}

	public String getUnit() {
		return this.unit;
	}
	
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public void setUnit() {
		if (this.unit == "°C") 
			this.unit = "°F";
		else if (this.unit == "°F")
			this.unit = "(arrondi)";
		else
			this.unit = "°C";
	}

	public double getFactor(double temp) {
		if (this.unit == "°C")
			this.factor = temp;
		else if (this.unit == "°F")
			this.factor = 1.8*temp + 32;
		else
			this.factor = Math.round(temp);
		
		return this.factor;
	}

}
