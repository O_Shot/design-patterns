package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensorDecorator;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) throws SensorNotActivatedException {
        ISensor sensor = new TemperatureSensorDecorator(true, 0);
        new MainWindow(sensor);
    }

}
